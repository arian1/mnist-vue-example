## Getting Started

#### Starting the Vue App

```bash
# install dependencies
$ npm install
```

##### With Yarn

```bash
$ yarn serve
```

##### With NPM

```bash
$ npm run-script serve
```

#### Serving models

```bash
# Using NPX Without installing local-web-server locally
$ npx local-web-server --index.root=saved_models
```

Or

```bash
# Install local-web-server and run
$ npm install -g local-web-server
$ ws --index.root=saved_models
```

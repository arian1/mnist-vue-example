import Vue from 'vue';
import Vuex from 'vuex';

import shared from './shared';
import image from './modules/image';

Vue.use(Vuex);

export default new Vuex.Store({
  debug: true,
  strict: true,
  modules: {
    shared,
    image,
  },
});

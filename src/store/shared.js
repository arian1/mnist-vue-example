const state = {
  autoLoad: false,
  autoEvaluate: false,
};

const getters = {
  autoLoad(state) {
    return state.autoLoad;
  },
  autoEvaluate(state) {
    return state.autoEvaluate;
  },
};

const mutations = {
  setAutoLoad(state) {
    state.autoLoad = !state.autoLoad;
  },
  setAutoEvaluate(state) {
    state.autoEvaluate = !state.autoEvaluate;
  },
};

const actions = {
  setAutoLoad({ commit }) {
    commit('setAutoLoad');
  },
  setAutoEvaluate({ commit }) {
    commit('setAutoEvaluate');
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};

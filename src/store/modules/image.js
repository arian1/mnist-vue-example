const state = {
  filename: '',
  imageBase64: '',
  imagePixels: [],
};

const getters = {
  filename(state) {
    return state.filename;
  },
  imageBase64(state) {
    return state.imageBase64;
  },
  imagePixels(state) {
    return state.imagePixels;
  },
  isImageReady(state) {
    return state.imageBase64.length > 0;
  },
};

const mutations = {
  setFilename(state, payload) {
    state.filename = payload;
  },
  setImageBase64(state, payload) {
    state.imageBase64 = payload;
  },
  setImagePixels(state, payload) {
    state.imagePixels = payload;
  },
  resetImageData(state) {
    state.imageBase64 = '';
    state.imagePixels = [];
    state.filename = '';
  },
};

const actions = {
  setFilename({ commit }, payload) {
    commit('setFilename', payload);
  },
  setImageBase64({ commit }, payload) {
    commit('setImageBase64', payload);
  },
  setImagePixels({ commit }, payload) {
    commit('setImagePixels', payload);
  },
  resetImageData({ commit }) {
    commit('resetImageData');
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};

import Vue from 'vue';
import App from './App.vue';
import SuiVue from 'semantic-ui-vue';

import store from './store';

import 'semantic-ui-css/semantic.min.css';
import './assets/global.css';

Vue.config.productionTip = false;
Vue.use(SuiVue);

new Vue({
  el: '#app',
  store,
  render: h => h(App),
});
